import { Component, OnInit } from '@angular/core';
import { TransferNow } from '../../model/transfer';
import { ActivatedRoute, Router } from '@angular/router';
import { TransfersNowService } from '../../services/transfers-now.service';
import { AccountsListComponent } from '../../accounts-list/accounts-list.component';

@Component({
  selector: 'app-transfer-now',
  templateUrl: './transfer-now.component.html',
  styleUrls: ['./transfer-now.component.scss']
})
export class TransferNowComponent implements OnInit {
  transfer: TransferNow;

  constructor(private route: ActivatedRoute, // injection d'activated route pr recuperer l'id de l'url
              private transfersService: TransfersNowService,
              private router: Router) { }

  ngOnInit(): void {
    this.transfer = new TransferNow('', '', '', ''); // on créé d'abord un transfer vide car la requête du serveur peut prendre du temps
    /* tslint:disable:no-string-literal */ // erreur avec params['id'] si on enlève ce commentaire
    const id = this.route.snapshot.params['id']; // on récupère l'id du transfer depuis l'url
    /* tslint:disable:no-string-literal */
    // on ajoute "+" devant l'id pour le cast entant que number afin
    // de pv recuperer le transfert
    this.transfersService.getSingleTransfer(+id).then(
      (transfer: TransferNow) => {
        this.transfer = transfer; // on stock le transfer ds la variable this.transfer
      }
    );
  }

  onBack(): any{
    this.router.navigate(['']).then(r => AccountsListComponent);
  }

}
