import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferNowComponent } from './transfer-now.component';

describe('TransferNowComponent', () => {
  let component: TransferNowComponent;
  let fixture: ComponentFixture<TransferNowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferNowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferNowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
