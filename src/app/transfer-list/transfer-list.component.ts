import {Component, OnDestroy, OnInit} from '@angular/core';
import { TransferNow } from '../model/transfer';
import { Subscription } from 'rxjs';
import { TransfersNowService } from '../services/transfers-now.service';
import { Router } from '@angular/router';
import firebase from 'firebase/compat';
import { AccountComponent } from '../accounts-list/account/account.component';

@Component({
  selector: 'app-transfer-list',
  templateUrl: './transfer-list.component.html',
  styleUrls: ['./transfer-list.component.scss']
})
export class TransferListComponent implements OnInit {
  isAuth: boolean;
  transfers: TransferNow[];
  transfersSubscription: Subscription;
  transfersUser: TransferNow[];

  constructor(private transfersService: TransfersNowService,
              private router: Router) { }

  ngOnInit(): void {
    this.isAuth = false;
    this.transfersSubscription = this.transfersService.transfersSubject.subscribe(
      (transfers: TransferNow[]) => {
        this.transfers = transfers;
      }
    );
    this.transfersService.getTransfers();
  }
  onRedirect(beneficiary): any{
    this.router.navigate(['/account/'] + beneficiary).then(r => AccountComponent);
  }
  // ngOnDestroy(): any{
  //  this.transfersService.transfersSubject.subscribe();
  // }
}
