import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { TransfersNowService } from '../../services/transfers-now.service';
import { TransferNow } from '../../model/transfer';
import { TransferListComponent } from '../transfer-list.component';
import { AccountsListComponent } from '../../accounts-list/accounts-list.component';
import { Account } from '../../model/account';
import { AccountsService } from '../../services/accounts.service';

@Component({
  selector: 'app-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss']
})
export class TransferFormComponent implements OnInit {
  transferForm: FormGroup;
  currentUser: Account;
  beneficiary: Account;

  constructor(private fB: FormBuilder,
              private transfersNowService: TransfersNowService,
              private router: Router,
              private accountsService: AccountsService) { }

  ngOnInit(): void {
      this.initForm();
      this.currentUser = this.accountsService.currentUser;
      this.beneficiary = this.transfersNowService.beneficiary;
      console.log(this.currentUser);
  }
  initForm(): void{
    this.transferForm = this.fB.group({
      amount: ['', Validators.required]
    });
  }
  onSubmitTransfer(): void{
    const date = Date.now();
    const beneficiary = this.beneficiary.name;
    const orderingAccount = this.currentUser.name;
    const amount = this.transferForm.get('amount').value;
    const newTransfer = new TransferNow(date, beneficiary, orderingAccount, +amount);
    console.log(newTransfer);
    this.transfersNowService.createNewTransfer(newTransfer);
    this.router.navigate(['/transfers']).then(r => TransferListComponent);
  }
  onRedirect(): any{
    this.router.navigate(['']).then(r => AccountsListComponent);
  }
}
