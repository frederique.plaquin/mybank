export class TransferNow {
  constructor(public date: any, public beneficiary: string, public orderingAccount: any, public amount: any) {
  }
}
