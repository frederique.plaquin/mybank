import { Pipe, PipeTransform } from '@angular/core';
import {Account} from '../model/account';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  transform(name: any): string {
    return name;
  }

}
