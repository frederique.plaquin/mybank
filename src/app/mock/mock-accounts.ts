import { Account } from '../model/account';

export const ACCOUNTS: Account[] = [
  {
    id: 1,
    name: 'Betty BOOP',
    initialAmount: 1462,
    interestRate: 5
  },
  {
    id: 2,
    name: 'Mary POPPINS',
    initialAmount: 659,
    interestRate: 15
  },
  {
    id: 3,
    name: 'Mickey MOUSE',
    initialAmount: 56555,
    interestRate: 20
  },
  {
    id: 4,
    name: 'Minnie MOUSE',
    initialAmount: 584,
    interestRate: 10
  },
  {
    id: 5,
    name: 'Donald DUCK',
    initialAmount: 958,
    interestRate: 20
  },
  {
    id: 6,
    name: 'Riri',
    initialAmount: 728,
    interestRate: 5
  },
  {
    id: 7,
    name: 'Fifi',
    initialAmount: 8528,
    interestRate: 15
  },
  {
    id: 8,
    name: 'Loulou',
    initialAmount: 85,
    interestRate: 5
  },
  {
    id: 9,
    name: 'Penny LANE',
    initialAmount: 226,
    interestRate: 10
  },
  {
    id: 10,
    name: 'Winnie LOURSON',
    initialAmount: 55,
    interestRate: 15
  },
  {
    id: 11,
    name: 'Jack THEFRIP',
    initialAmount: 5895,
    interestRate: 20
  },
  {
    id: 12,
    name: 'Mufasa LION',
    initialAmount: 22,
    interestRate: 15
  },
  {
    id: 13,
    name: 'Jean-Paul DUSS',
    initialAmount: 2829,
    interestRate: 10
  },
  {
    id: 14,
    name: 'Mickael JACKSON',
    initialAmount: 5895,
    interestRate: 20
  },
  {
    id: 15,
    name: 'Elvis PRESLEY',
    initialAmount: 55625,
    interestRate: 10
  }
];
