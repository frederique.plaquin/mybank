import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SigninComponent } from '../auth/signin/signin.component';
import { SignupComponent } from '../auth/signup/signup.component';
import firebase from 'firebase/compat/app';
import 'firebase/compat/database'; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';
import { AuthService } from '../services/auth.service';
import { TransferNowComponent } from '../transfer-list/transfer-now/transfer-now.component';
import { AccountComponent } from '../accounts-list/account/account.component';
import { AccountsListComponent } from '../accounts-list/accounts-list.component';
import {Account} from '../model/account';
import {AccountsService} from '../services/accounts.service';
import { DatasService } from '../services/datas.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isAuth: boolean;
  currentUser: Account;
  currentUserId: any;

  constructor(private router: Router,
              private authService: AuthService,
              private accountsService: AccountsService) { }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged( // à chaque du dtatus de connexion de l'utilisateur
      (user) => {
        this.isAuth = !!user; // si utilisateur connecté alors isAuth = true
        this.currentUserId = user.uid;
        this.getUserAccount(this.currentUserId);
      }
    );
  }
  getUserAccount(userId): any{
    this.accountsService.getSingleAccount(userId).then(
      () => {
        document.getElementById('current-user-name').innerText = this.accountsService.currentUser.name;
      }
    );
  }
  onSignOut(): any{
    this.authService.signOutUser();
  }

  signIn(): any{
    this.router.navigate(['/signIn']).then(r => SigninComponent);
  }
  signUp(): any{
    this.router.navigate(['/signUp']).then(r => SignupComponent);
  }

  onRedirect(destination): any{
    if (destination === 'transfer') {
      this.router.navigate(['/transferNow']).then(r => TransferNowComponent);
    } else if (destination === 'home') {
      this.router.navigate(['']).then(r => AccountsListComponent);
    } else if (destination === 'myAccount'){
      this.router.navigate(['/myAccount', +this.currentUser.id]).then(r => AccountComponent);
    }
  }

}
