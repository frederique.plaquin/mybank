import { Component } from '@angular/core';
// Import the functions you need from the SDKs you need

// Your web app's Firebase configuration
// Import the functions you need from the SDKs you need
import { getFirestore} from 'firebase/firestore/lite';

import firebase from 'firebase/compat/app';
import 'firebase/compat/database'; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';
import { AccountsService } from './services/accounts.service';

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private accountsService: AccountsService) {
// Your web app's Firebase configuration

    const firebaseConfig = {
      apiKey: 'AIzaSyCHti1Oo3gFO9XB6WQo_MMPqj9gJRfj0yA',
      authDomain: 'mybank-aa13f.firebaseapp.com',
      projectId: 'mybank-aa13f',
      databaseURL: 'https://mybank-aa13f-default-rtdb.europe-west1.firebasedatabase.app/',
      storageBucket: 'mybank-aa13f.appspot.com',
      messagingSenderId: '900505742487',
      appId: '1:900505742487:web:3303fad8d3a3de38c38dd2'
    };


// Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    const db = getFirestore();
  }
}
