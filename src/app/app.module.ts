import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MdbModule } from 'mdb-angular-ui-kit';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { NewAccountComponent } from './accounts-list/new-account/new-account.component';
import { UsersListComponent } from './users-list/users-list.component';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { Account } from './model/account';
import { AccountsListComponent } from './accounts-list/accounts-list.component';
import { AccountFormComponent } from './accounts-list/account-form/account-form.component';
import { TransferNowComponent } from './transfer-list/transfer-now/transfer-now.component';
import { AuthService } from './services/auth.service';
import { AuthComponent } from './auth/auth.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthGuardService } from './services/auth-guard.service';
import { TransfersNowService } from './services/transfers-now.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SigninComponent } from './auth/signin/signin.component';
import { UserComponent } from './users-list/user/user.component';
import { AccountComponent } from './accounts-list/account/account.component';
import { TransferFormComponent } from './transfer-list/transfer-form/transfer-form.component';
import { TransferListComponent } from './transfer-list/transfer-list.component';
import { HttpClientModule } from '@angular/common/http';
import { NamePipe } from './pipes/name.pipe'; // va nous servir à enregistrer les données sur la BDD du serveur Firebase

const appRoutes: Routes = [
  { path: '', component: AccountsListComponent },
  { path: 'signIn', component: SigninComponent },
  { path: 'signUp', component: SignupComponent },
  // { path: 'auth', component: AuthComponent },
  { path: 'transferNow', canActivate: [AuthGuardService], component: TransferNowComponent },
  { path: 'myAccount', canActivate: [AuthGuardService], component:  AccountComponent },
  { path: 'accounts', component: AccountsListComponent },
  { path: 'user', canActivate: [AuthGuardService], component: UserComponent },
  { path: 'transfers', component: TransferListComponent },
  { path: '**', component: NotFoundComponent }
  // { path: 'signup', component: SignupComponent },
  // { path: 'auth', component: AuthComponent },
  // { path: 'books', canActivate: [AuthGuardService] , component: BookListComponent },
  // { path: 'books/new', canActivate: [AuthGuardService] , component: BookFormComponent },
  // { path: 'books/view/:id', canActivate: [AuthGuardService] , component: SingleBookComponent },
  // affichage par default si rien n'est spécifié ds l url + pathMatch: 'full' évite erreurs de redirection
  // { path: '', redirectTo: 'accounts-list', pathMatch: 'full' },
  // { path: 'transfer', canActivate: [AuthGuardService] , component: TransferNowComponent },
  // path wildcard tjs à placer à la fin c'est l'affichage par default si rien url incorrect
  // { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewAccountComponent,
    AccountsListComponent,
    UsersListComponent,
    NotFoundComponent,
    AccountFormComponent,
    TransferNowComponent,
    AuthComponent,
    SignupComponent,
    SigninComponent,
    UserComponent,
    AccountComponent,
    TransferFormComponent,
    TransferListComponent,
    NamePipe,
  ],
  imports: [
    BrowserModule,
    MdbModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    TransfersNowService,
    AuthService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
