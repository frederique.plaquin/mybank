import { Component, OnInit } from '@angular/core';
import { Account } from '../../model/account';
import { AccountsService } from '../../services/accounts.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  currentUser: Account;

  constructor(private accountsService: AccountsService) { }

  ngOnInit(): void {
    this.currentUser = this.accountsService.currentUser;
    document.getElementById('current-user-name').innerText = this.currentUser.name;
  }

}
