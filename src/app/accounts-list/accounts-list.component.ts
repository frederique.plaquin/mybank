import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Account } from '../model/account';
import { AccountsService } from '../services/accounts.service';
import { Router } from '@angular/router';
import { TransferNowComponent } from '../transfer-list/transfer-now/transfer-now.component';
import { AuthService } from '../services/auth.service';
import { AuthComponent} from '../auth/auth.component';
import {TransfersNowService} from '../services/transfers-now.service';
import firebase from 'firebase/compat';
import User = firebase.User;

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit, OnDestroy {
  accounts: Account[]; // creation de l'array local Accounts
  accountsSubscription: Subscription; // soubscription au sujet
  authStatus: boolean;

  constructor(private accountService: AccountsService,
              private authService: AuthService,
              private transfersService: TransfersNowService,
              private router: Router) { }

  ngOnInit(): void {
    this.authStatus = this.authService.isAuth;
    this.accountsSubscription = this.accountService.accountSubject.subscribe(
      (accounts: Account[]) => {
        this.accounts = accounts;
      }
    );
    this.accountService.getAccountsList();
  }
  onNewAccount(): void{
    this.router.createUrlTree(['/accounts', 'new']);
  }
  onDeleteAccount(account: Account): void{
    this.accountService.removeAccount(account);
  }
  redirectToTransferNow(beneficiary): void{
    this.transfersService.setTransfertBeneficiary(beneficiary);
    this.router.navigate(['./transferNow/']).then(r => TransferNowComponent);
  }
  ngOnDestroy(): void{
    this.accountsSubscription.unsubscribe();
  }
}
