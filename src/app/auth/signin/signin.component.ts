import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {AccountsListComponent} from '../../accounts-list/accounts-list.component';
import {AccountComponent} from "../../accounts-list/account/account.component";
import {AccountsService} from "../../services/accounts.service";
import firebase from "firebase/compat/app";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  signInForm: FormGroup;
  errorMessage: string;
  constructor(private fB: FormBuilder,
              private authService: AuthService,
              private accountsService: AccountsService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(): any{
    this.signInForm = this.fB.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }
  onSubmit(): any{
    const email = this.signInForm.get('email').value;
    const password = this.signInForm.get('password').value;
    this.authService.signInUser(email, password).then(
      () => {
        this.authService.setCurrentUser();
        const userId = firebase.auth().currentUser.uid;
        this.router.navigate(['/myAccount', userId]);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }
}
