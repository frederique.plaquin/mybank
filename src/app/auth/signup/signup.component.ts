import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {AccountsListComponent} from '../../accounts-list/accounts-list.component';
import {AccountComponent} from '../../accounts-list/account/account.component';
import {AccountsService} from '../../services/accounts.service';
import {create} from 'domain';
import firebase from 'firebase/compat/app';
import 'firebase/compat/database'; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';
import {Account} from '../../model/account';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;
  userMessage: string;
  errorMessage: string;
  newAccount: Account;

  constructor(private fB: FormBuilder,
              private authService: AuthService,
              private accountsService: AccountsService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(): any{
    this.signUpForm = this.fB.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }
  onSubmit(): any{
    this.accountsService.getAccounts();
    const name = this.signUpForm.get('firstname').value + ' ' + this.signUpForm.get('lastname').value;
    const email = this.signUpForm.get('email').value;
    const password = this.signUpForm.get('password').value;
    this.authService.createNewUser(email, password, name).then(
      () => {
        this.accountsService.createNewAccount(name);
        this.router.navigate(['/myAccount/']).then(r => AccountComponent);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }
}
