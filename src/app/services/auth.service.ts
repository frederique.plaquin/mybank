import { Injectable } from '@angular/core';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import firebase from 'firebase/compat/app';
import { AccountsListComponent } from '../accounts-list/accounts-list.component';
import { Account } from '../model/account';
import { HttpClient } from '@angular/common/http';
import {Subject} from 'rxjs';
import { AccountsService } from './accounts.service';
import { DatasService } from './datas.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth = false;
  currentUserId: any;
  currentUser: Account;
  // isFirstTime = false;
  // newUserName = '';

  constructor(private httpClient: HttpClient,
              private accountsService: AccountsService,
              private dataService: DatasService) {
  }
  createNewUser(email: string, password: string, name: string): any {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => {
            resolve(resolve);
          },
          (error: any) => {
            reject(error);
          }
        );
      }
    );
  }

  signInUser(email: string, password: string): any {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password).then(
          () => {
            resolve(resolve);
            this.currentUserId = firebase.auth().currentUser.uid;
            console.log(firebase.auth().currentUser.uid);
            this.dataService.setOption('currentUserId', this.currentUserId);
          },
          (error: any) => {
            reject(error);
          }
        );
      }
    );
  }
  setCurrentUser(): any{
    const id = firebase.auth().currentUser.uid;
    this.accountsService.getSingleAccount(id).then(
      (account: any) => {
        console.log('auth 1: ');
        console.log(account);
        this.currentUser = new Account(account.id, account.name, account.amount, account.rate);
        this.dataService.setOption('currentUser', this.currentUser);
        console.log('auth 1: current user - ');
        console.log(this.currentUser);
        // ici comme la requête arrive plus tard on ne  peut pas passer par {{}} ds le header.html
      });
  }

  signOutUser(): any {
    firebase.auth().signOut().then(r => AccountsListComponent);
  }

}
