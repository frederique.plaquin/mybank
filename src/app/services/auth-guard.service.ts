import { Injectable } from '@angular/core';

import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs'; // s'ajoute auto qd on "implements CanActivate" et private router: Router
import firebase from 'firebase/compat/app';
import { SigninComponent } from '../auth/signin/signin.component';

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      ((resolve, reject) => {
        firebase.auth().onAuthStateChanged(
          (user) => {
            if (user) {
              resolve(true);
            } else {
              this.router.navigate(['signIn']).then(r => SigninComponent);
              resolve(false);
            }
          }
        );
      })
    );
  }
}

