import { Injectable } from '@angular/core';
import { TransferNow } from '../model/transfer';
import { Subject } from 'rxjs';
import firebase from 'firebase/compat/app';
import 'firebase/compat/database'; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';
import { HttpClient } from '@angular/common/http';
import { error } from 'protractor';
import {Account} from '../model/account';
import { AccountsService } from './accounts.service'; // va nous servir à stocker données ds BDD Firebase

@Injectable({
  providedIn: 'root'
})
export class TransfersNowService {
  transfers: TransferNow[] = []; // array local
  transfersSubject = new Subject<TransferNow[]>(); // on créé un subject qui émet l'array TransferNow[]
  userMessage: string;
  beneficiary: Account;

  constructor(private httpClient: HttpClient,
              private accountsService: AccountsService) { } // injection HttpCLient

  // méthode qui émet le Subject
  emitTransfers(): any{
    this.transfersSubject.next(this.transfers);
  }
  saveTransfers(): any{
    firebase.database().ref('accounts/').child(this.beneficiary.id).update({initialAmount: this.beneficiary.initialAmount}).then(
      () => {
        console.log('LE compte du bénéficiaire a bien été mis à jour.');
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        console.log(error);
      }
    );
    firebase.database().ref('accounts/').child(this.accountsService.currentUser.id).update({
      initialAmount: this.accountsService.currentUser.initialAmount
    }).then(
      () => {
        console.log('Le compte du donneur d\'ordre a bien été mis à jour.');
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        console.log(error);
      }
    );
    firebase.database().ref('transfers/').update({
      initialAmount: this.accountsService.currentUser.initialAmount
    }).then(
      () => {
        console.log('Le compte du donneur d\'ordre a bien été mis à jour.');
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        console.log(error);
      }
    );
    firebase.database().ref('/transfers').set(this.transfers).then(
      () => {
        console.log('enregistrement réussi');
      },
      // tslint:disable-next-line:no-shadowed-variable
      (error) => {
        console.log('l\'enregistrement a échoué' + error);
      }
    );
    // on créé le node de la BDD (/transfers) puis on enregistre notre array ds le node
  }
  getTransfers(): any{
    firebase.database().ref('/transfers')
      .on('value', (data) => { // .on va verifier l etat de la database en permanence contrairement à .once
        this.transfers = data.val() ? data.val() : []; // operateur terniare '?' permet de dire que variable peut être undefined
        this.emitTransfers();
      }); // .on() permet de réagir aux modifications de la bdd
  }
  getSingleTransfer(id: number): any{
    return new Promise(
      ((resolve, reject) => {
        firebase.database().ref('/transfers/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          },
          // tslint:disable-next-line:no-shadowed-variable
          (error) => {
            reject(error);
          }
        );
      })
    );
  }
  createNewTransfer(newTransfer: TransferNow): any{
    this.makeTransfer(newTransfer.amount);
    this.transfers.push(newTransfer);
    this.saveTransfers();
    this.emitTransfers();
  }
  setTransfertBeneficiary(beneficiary: Account): any{
    this.beneficiary = new Account(beneficiary.id, beneficiary.name, +beneficiary.initialAmount, +beneficiary.interestRate);
  }
  makeTransfer(amount): any{
    amount = +amount;
    const beneficiaryInitialAmount = +this.beneficiary.initialAmount;
    const orderingAccountInitialAmount = this.accountsService.currentUser.initialAmount;
    this.beneficiary.initialAmount = beneficiaryInitialAmount + amount;
    this.accountsService.currentUser.initialAmount = orderingAccountInitialAmount - amount;
  }
}
