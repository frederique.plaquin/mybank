import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatasService {

  private data = {};

  setOption(option, value): any{
    this.data[option] = value;
  }

  getOption(): any{
    return this.data;
  }
}
