import { TestBed } from '@angular/core/testing';

import { TransfersNowService } from './transfers-now.service';

describe('TransfersServiceService', () => {
  let service: TransfersNowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransfersNowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
