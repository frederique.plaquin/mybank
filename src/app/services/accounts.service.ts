import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Account } from '../model/account';
import { ACCOUNTS } from '../mock/mock-accounts';
import {element} from 'protractor';
import firebase from 'firebase/compat/app';
import 'firebase/compat/database'; // "firebase/compat/database" et nn "firebase/database"
import 'firebase/compat/storage';
import { HttpClient } from '@angular/common/http';
import { DatasService } from './datas.service';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  accounts: Account[] = [] ;
  bddAccounts: Account[] = [] ;
  accountSubject = new Subject<Account[]>();
  currentUser: Account;

  constructor(private httpClient: HttpClient,
              private dataService: DatasService) { }
  emitAccounts(): void{
    this.accountSubject.next(this.accounts);
  }
  saveAccount(): void{
    this.accounts = ACCOUNTS;
    this.accounts.forEach(
      (item) => {
        firebase.database().ref('/accounts/' + item.id)
          .set({
            id: item.id,
            name: item.name,
            initialAmount: item.initialAmount,
            interestRate: item.interestRate
          }).then(r => console.log('its okay'));
      }
    );
  }
  getAccounts(): any{
    this.accounts = ACCOUNTS;
    firebase.database().ref('/accounts/')
      .on('value', (data) => {
        this.bddAccounts = data.val() ? data.val() : []; // operateur terniare '?' permet de dire que variable peut être undefined
        this.bddAccounts.forEach(
          (account, index) => {
            const isAccount = this.accounts.filter(item => item.id === account.id);
            if (!isAccount) {
              this.accounts.push(account);
            }
          }
        );
        this.emitAccounts();
      }); // .on() permet de réagir aux modifications de la bdd
  }
  getAccountsList(): any{
    this.accounts = ACCOUNTS;
    firebase.database().ref('/accounts')
      .on('value', (data) => {
        this.bddAccounts = data.val() ? data.val() : []; // operateur terniare '?' permet de dire que variable peut être undefined
        if (this.bddAccounts.length === 0){
          this.accounts = ACCOUNTS;
        } else {
          const arr = [];
          // tslint:disable-next-line:only-arrow-functions
          Object.keys(this.bddAccounts).map(function(key): any{
            const user = new Account(data.val()[key].id, data.val()[key].name, +data.val()[key].initialAmount,
              +data.val()[key].interestRate);
            arr.push(user);
            return arr;
          });
          this.accounts = arr;
        }
        this.emitAccounts();
      }); // .on() permet de réagir aux modifications de la bdd
  }
  createNewAccount(name): any{
    const id = firebase.auth().currentUser.uid;
    const amount = Math.floor(Math.random() * (20000 + 1));
    const rate = Math.floor(Math.random() * (20 - 5 + 1)) - 5;
    const newAccount = new Account(id, name, amount, rate);
    this.accounts.push(newAccount);
    this.saveAccount();
    this.emitAccounts();
  }

  removeAccount(account: Account): void{
    // tslint:disable-next-line:no-shadowed-variable
    this.accounts.forEach((element, index) => {
      if (element[index].id === account[index].id){
        delete this.accounts[index];
      }
    });
  }
  getSingleAccount(id: any): any{
    return new Promise(
      ((resolve, reject) => {
        firebase.database().ref('/accounts/').once('value').then(
          // on utilise once() car on a besoin de retourner les données qu'une fois contrairement à on()
          (data) => {
            data.forEach(
              (item) => {
                  const itemId = item.val().id;
                  if (itemId === id){
                    resolve(item.val()); // ici si résultat data.val va se remplir avec la valeur de la donnée
                    this.currentUser = new Account(item.val().id, item.val().name, item.val().initialAmount, item.val().interestRate);
                  }
              });
          },
          (error) => {
            reject(error); // sinon on reject avec erreur en argument
          }
        );
      })
    );
  }
}
